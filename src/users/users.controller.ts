import { Body, Controller, FileTypeValidator, Get, MaxFileSizeValidator, Param, ParseFilePipe, ParseIntPipe, Post, Put, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UsersService } from './users.service';
import { JwtAuthGuard } from 'src/auth/jwt/jwt-auth.guard';
import { UpdateUserDto } from './dto/update-user.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { JwtRolesGuard } from 'src/auth/jwt/jwt-roles.guard';
import { HasRoles } from 'src/auth/jwt/has-roles';
import { JwtRole } from 'src/auth/jwt/jwt-rol';

@Controller('users')
export class UsersController {

    constructor(private usersService: UsersService){}

    //get obtener
    //post crear 
    //put path actualizar 
    //delete eliminar
    @HasRoles(JwtRole.ADMIN)
    @UseGuards(JwtAuthGuard,JwtRolesGuard)
    @Get()//http://192.168.68.55:3000/users ->get
    findAll(){
        return this.usersService.findAll();
    }

    @UseGuards(JwtAuthGuard)
    @Post()//http://192.168.68.55:3000/users ->post
    create(@Body() user:CreateUserDto){
        console.log("pasa create")
        return this.usersService.create(user);
    }

    
    @UseGuards(JwtAuthGuard)
    @Put(":id")//http://192.168.68.55:3000/users/:id ->post
    update(@Param("id",ParseIntPipe) id:number, @Body() user:UpdateUserDto){
        return this.usersService.update(id,user);
    }

    @HasRoles(JwtRole.CLIENT, JwtRole.ADMIN)
    @UseGuards(JwtAuthGuard,JwtRolesGuard)
    @Post('upload/:id')//http://192.168.68.55:3000/users/upload ->post
    @UseInterceptors(FileInterceptor('file'))
    updateImage(
        @Param("id",ParseIntPipe) id:number, @Body() user:UpdateUserDto,
        @UploadedFile(
        new ParseFilePipe({
            validators: [
            new MaxFileSizeValidator({ maxSize: 1024 * 1024 * 5 }),
            new FileTypeValidator({ fileType: '.(png|jpeg|jpg)' }),
            ],
        })) file: Express.Multer.File) {
       return  this.usersService.updateWhitImage(file,id,user)
    }

}
