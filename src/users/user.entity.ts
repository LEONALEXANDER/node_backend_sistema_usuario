
import { Entity, PrimaryGeneratedColumn,Column, BeforeInsert, ManyToMany, JoinTable } from "typeorm";
import { hash } from "bcrypt";
import { Rol } from "src/roles/roles.entity";

@Entity({name:"users"})
export class User{
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    name:string;

    @Column()
    lastname:string;

    @Column({unique:true})
    email:string;

    @Column({unique:true})
    phone:string;

    @Column({nullable:true})
    image:string;

    @Column({nullable:true})
    password:string;

    @Column()
    notification_token:string;

    @Column({type:"datetime",default: () => 'CURRENT_TIMESTAMP' })
    created_at:Date;

    @Column({type:"datetime",default: () => 'CURRENT_TIMESTAMP' })
    updated_at:Date;

    @Column({default:true})
    active:boolean;

    @JoinTable({
        name:"user_has_roles",
        joinColumn:{
            name:"id_user"
        },
        inverseJoinColumn:{
            name:"id_rol"
        }
    })
    @ManyToMany(()=>Rol,(rol)=>rol.users)
    roles:Rol[]

    @BeforeInsert()
    async hasPaswoed(){
        this.password = await hash(this.password,Number(process.env.HASH_SALT))
    }

    
}

