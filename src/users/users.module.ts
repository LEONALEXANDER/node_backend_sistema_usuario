import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './user.entity';
import { JwtStrategy } from 'src/auth/jwt/jwt.strategy';
import { Rol } from 'src/roles/roles.entity';
import { RolesService } from 'src/roles/roles.service';

@Module({
  imports:[TypeOrmModule.forFeature([User,Rol])],
  providers: [UsersService,RolesService,JwtStrategy],
  controllers: [UsersController]
})
export class UsersModule {}
